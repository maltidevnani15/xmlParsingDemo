package com.xmlparsingdemo;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.xmlparsingdemo.databinding.ActivityMainBinding;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    private String LOG_TAG = "XML";
    private int UpdateFlag = 0;
    private ArrayList<Data> datalist;
    Data data;
    private ActivityMainBinding activityBinding;
    private DataAdapter dataAdapter;
    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        datalist = new ArrayList<>();
        dataAdapter = new DataAdapter(this, datalist);
        activityBinding.recycleView.setLayoutManager(new LinearLayoutManager(this));
        activityBinding.recycleView.setAdapter(dataAdapter);
        new GetXMLFromServer().execute();
    }

    public void ParseXML(String xmlString) {
        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xmlString));
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("Horoscope")) {
                            // create a new instance of employee
                            data = new Data();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("Horoscope")) {
                            // add employee object to list
                            datalist.add(data);
                        } else if (tagname.equalsIgnoreCase("Name")) {
                            data.setName(text);
                        } else if (tagname.equalsIgnoreCase("Range")) {
                            data.setRange(text);
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            Log.d(LOG_TAG, "Error in ParseXML()", e);
        }
    }

    private class GetXMLFromServer extends AsyncTask<String, Void, String> {

        HttpHandler nh;

        @Override
        protected String doInBackground(String... strings) {

            String URL = "http://androidpala.com/tutorial/horoscope.xml";
            String res = "";
            nh = new HttpHandler();
            InputStream is = nh.CallServer(URL);
            if (is != null) {

                res = nh.StreamToString(is);

            } else {
                res = "NotConnected";
            }

            return res;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equals("NotConnected")) {
                Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_SHORT).show();
            } else {
                ParseXML(result);
//                ParsingByDom(result);
                Log.e("list size", String.valueOf(datalist.size()));
                setData();
            }
        }


    }

    private void setData() {
        dataAdapter.notifyDataSetChanged();
    }



}



