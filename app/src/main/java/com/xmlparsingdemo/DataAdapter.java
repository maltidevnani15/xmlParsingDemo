package com.xmlparsingdemo;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xmlparsingdemo.databinding.DataRowBinding;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private DataRowBinding rowBinding;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Data> dataArrayList;
    public DataAdapter(final Context context,final ArrayList<Data> dataArrayList){
        this.context=context;
        this.dataArrayList=dataArrayList;
    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=LayoutInflater.from(context);
        rowBinding= DataBindingUtil.inflate(inflater, R.layout.data_row,parent,false);
        return new ViewHolder(rowBinding);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        holder.rowAdapterBinding.rowName.setText(dataArrayList.get(position).getName());
        holder.rowAdapterBinding.rowRange.setText(dataArrayList.get(position).getRange());

    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        DataRowBinding rowAdapterBinding;
        public ViewHolder(DataRowBinding rowAdapterBinding) {
            super(rowAdapterBinding.getRoot());
            this.rowAdapterBinding=rowAdapterBinding;

        }

    }
}
